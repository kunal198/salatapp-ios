//
//  SlectedViewController.m
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "SlectedViewController.h"
#import "SetAlarmForPrayerViewController.h"
#import "UserNameViewController.h"
#import "PrayertimeViewController.h"
#import "CompassViewController.h"
#import "SettingViewController.h"
@interface SlectedViewController ()

@end

@implementation SlectedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = true;
    self.PreviousPrayerLbl.layer.cornerRadius = 20;
    self.PreviousPrayerLbl.layer.masksToBounds = YES;
    self.PreviousPrayerLbl.layer.borderWidth = 2;
    self.PreviousPrayerLbl.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    
    self.WrapperViewNextPrayer.layer.cornerRadius = 20;
    self.WrapperViewNextPrayer.layer.masksToBounds = YES;
    self.WrapperViewNextPrayer.layer.borderWidth = 2;
    self.WrapperViewNextPrayer.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    self.WrapperViewPreviousPrayer.layer.cornerRadius = 20;
    self.WrapperViewPreviousPrayer.layer.masksToBounds = YES;
    self.WrapperViewPreviousPrayer.layer.borderWidth = 2;
    self.WrapperViewPreviousPrayer.layer.borderColor = [UIColor colorWithRed:242.0/255 green:229.0/255 blue:206.0/255 alpha:1.0].CGColor;
    
    self.menuhideoutlet.hidden = YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.showmenuview.hidden = YES;
    self.menuhideoutlet.hidden = YES;
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)menubtn:(id)sender
{
    
    self.showmenuview.hidden = NO;
    self.menuhideoutlet.hidden = NO;
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = 0;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];

}

- (IBAction)setlocation:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (IBAction)compassbtn:(id)sender

{
    CompassViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"CompassViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    

}

- (IBAction)weeklytablebtn:(id)sender
{
    PrayertimeViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"PrayertimeViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
}

- (IBAction)loginbtn:(id)sender
{
    UserNameViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"UserNameViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
}

- (IBAction)settingbtn:(id)sender
{
    SettingViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
 
}

- (IBAction)nextbtn:(id)sender
{
    
    SetAlarmForPrayerViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"SetAlarmForPrayerViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
}

- (IBAction)menuhidebtn:(id)sender
{
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = -265;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
     self.menuhideoutlet.hidden = YES;

}
@end
