//
//  ContactlistTableViewCell.h
//  SalatApp
//
//  Created by brst on 14/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactlistTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *unmarkimage;
@property (strong, nonatomic) IBOutlet UIImageView *markimage;

@property (strong, nonatomic) IBOutlet UIButton *checkimagebtn;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *mobilenumber;

@end
