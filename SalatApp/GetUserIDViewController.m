//
//  GetUserIDViewController.m
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "GetUserIDViewController.h"
#import "AllPrayersViewController.h"
@interface GetUserIDViewController ()

@end

@implementation GetUserIDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.navigationController.navigationBarHidden = true;
    
    UINavigationBar *bar = [self.navigationController navigationBar];
    [bar setBarTintColor:[UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0]];
    [bar setTintColor:[UIColor whiteColor]];
    
    self.EnterBtn.layer.cornerRadius = 20;
    self.EnterBtn.layer.masksToBounds = YES;
    self.EnterBtn.layer.borderWidth = 2;
    self.EnterBtn.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
     self.navigationItem.title = @"Prayer Snooze Social";
    self.navigationItem.backBarButtonItem.title=@"Back";
    
    
//    dispatch_async(dispatch_get_main_queue(), ^(void) {
//        self.navigationController.navigationBar.backItem.title = @"Back";
//    });

}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)EnterBtn:(id)sender
{
    AllPrayersViewController *userID = [self.storyboard instantiateViewControllerWithIdentifier:@"AllPrayersViewController"];
    [self.navigationController pushViewController:userID animated:NO];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
