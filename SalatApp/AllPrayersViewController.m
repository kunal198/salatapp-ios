//
//  AllPrayersViewController.m
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "AllPrayersViewController.h"
#import "AllPrayersTableViewCell.h"
#import "SlectedViewController.h"
#import "PrayertimeViewController.h"
#import "UserNameViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CompassViewController.h"
#import "SettingViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "AppDelegate.h"


@interface AllPrayersViewController ()

@end

@implementation AllPrayersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self ConsultantAllQualificationsList];
    
    
    
    
   self.navigationController.navigationBarHidden = true;
    AllPrayersName = [[NSMutableArray alloc] init];
    AllPrayerTime = [[NSMutableArray alloc] init];
    
    AllPrayersName = [NSMutableArray arrayWithObjects:@"Fajr",@"Sunrise",@"Dhuhr",@"Asr",@"Sunset",@"Maghrib",@"Isha",nil];
    
     AllPrayerTime = [NSMutableArray arrayWithObjects:@"05:23",@"06:26",@"12:27",@"15:53",@"18:29",@"18:29",@"19:32",nil];
    
    self.showmenuview.hidden = YES;
    self.hidemenuview.hidden = YES;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    geocoder = [[CLGeocoder alloc] init];
    locationFetchCounter = 0;
    
    // fetching current location start from here
    [locationManager startUpdatingLocation];

}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // this delegate method is constantly invoked every some miliseconds.
    // we only need to receive the first response, so we skip the others.
    if (locationFetchCounter > 0) return;
    locationFetchCounter++;
    
    // after we have current coordinates, we use this method to fetch the information data of fetched coordinate
    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks lastObject];
        
        NSString *street = placemark.thoroughfare;
        NSString *city = placemark.locality;
        NSString *posCode = placemark.postalCode;
        NSString *country = placemark.country;
        
        NSLog(@"we live in %@", city);
        
        // stopping locationManager from fetching again
        [locationManager stopUpdatingLocation];
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"failed to fetch current location : %@", error);
}

-(void)viewWillAppear:(BOOL)animated
{
        
       self.showmenuview.hidden = YES;
    self.hidemenuview.hidden = YES;

}



-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [AllPrayersName count];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllPrayersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
   // cell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableView.separatorColor=[UIColor colorWithRed:146.0/255 green:66.0/255 blue:36.0/255 alpha:1.0];
    
    cell.PrayerNameLbl.text = AllPrayersName[indexPath.row];
    cell.PrayerTimeLbl.text = AllPrayerTime[indexPath.row];
    cell.WrapperView.hidden = true;

     cell.ring.hidden = false;
    cell.PrayerNameLbl.highlightedTextColor = [UIColor whiteColor];
    cell.PrayerTimeLbl.highlightedTextColor = [UIColor whiteColor];

    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:147.0/255.0 green:66.0/255.0 blue:36.0/255.0 alpha:1.0];

    
    [cell setSelectedBackgroundView:bgColorView];
    
    if (indexPath.row == 0)
    {
        cell.highlightring.hidden = false;
        cell.ring.hidden = true;
        cell.dotlabel.hidden = true;
        
    }
   else if (indexPath.row == 1)
    {
        cell.highlightring.hidden = true;
        cell.ring.hidden = true;
        cell.dotlabel.hidden = false;
        
    }
   else if (indexPath.row == 2)
    {
        cell.highlightring.hidden = true;
        cell.ring.hidden = true;
        cell.dotlabel.hidden = false;
        
    }
    
  else  if (indexPath.row == 3)
    {
        cell.highlightring.hidden = true;
        cell.ring.hidden = false;
        cell.dotlabel.hidden = true;
        
    }
    
   else if (indexPath.row == 4)
    {
        cell.highlightring.hidden = true;
        cell.ring.hidden = true;
        cell.dotlabel.hidden = false;

        
    }
    
   else if (indexPath.row == 5)
    {
        cell.highlightring.hidden = true;
        cell.ring.hidden = true;
        cell.dotlabel.hidden = false;
    }
    
   else if (indexPath.row == 6)
   {
       cell.highlightring.hidden = false;
       cell.ring.hidden = true;
       cell.dotlabel.hidden = true;
   }
   else{
       
   }

    return cell;
}

#pragma AFNetworking

-(void)ConsultantAllQualificationsList
{

    if ([(AppDelegate*)[[UIApplication sharedApplication] delegate] internetActive])
    {
//        spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyle9CubeGrid color:[UIColor whiteColor]];
//
//        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        hud.square = YES;
//        hud.mode = MBProgressHUDModeCustomView;
//        hud.customView = spinner;
//        hud.labelText = NSLocalizedString(@"Loading", @"Loading");
//
//        [spinner startAnimating];

        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

        
        
        NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
        NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        NSLog(@"%@",countryCode);
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MMM"];
        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);


        NSString *currentdate = [dateFormatter stringFromDate:[NSDate date]];
        NSLog(@"curresnt date=%@",currentdate);
        
        NSArray *Date = [currentdate componentsSeparatedByString:@"-"];
        
        
        
//        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
//        
//        NSInteger day = [components day];
//        NSInteger week = [components month];
//        NSInteger year = [components year];
//        
//        NSString *string = [NSString stringWithFormat:@"%ld-%ld-%ld", (long)day, (long)week, (long)year];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"d-MMM-yyyy";
        NSString *Datestring = [formatter stringFromDate:[NSDate date]];
        
//        NSLog(@"date is%@",string);
         NSLog(@"date is%@",Datestring);
        
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        
        [df setDateFormat:@"dd"];
        NSString *myDayString = [df stringFromDate:[NSDate date]];
        
        [df setDateFormat:@"MMM"];
        NSString *myMonthString = [df stringFromDate:[NSDate date]];
        
        [df setDateFormat:@"yy"];
        NSString *myYearString = [df stringFromDate:[NSDate date]];
        NSLog(@"date is%@",myDayString);
        NSLog(@"date is%@",myMonthString);
        NSLog(@"date is%@",myYearString);
        
       strUrlLink=[NSString stringWithFormat:@"http://api.aladhan.com/timingsByCity?city=%@&country=%@&method=%@",city,countryCode,Datestring,@"2"];

        strUrlLink = [strUrlLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];


        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]init];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
        [manager POST:strUrlLink parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {

             if ([[responseObject valueForKey:@"result"] integerValue])
             {

                   NSLog(@"responseObject = %@",responseObject);



             }
             else
             {
                 //NSLog(@"responseObject = %@",[responseObject valueForKey:@"message"]);

                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];

                 UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];

                 [alertController addAction:ok];

                 [self presentViewController:alertController animated:YES completion:nil];


             }


            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
//             [spinner stopAnimating];
//             [hud hide:YES];

         }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  self.view.userInteractionEnabled=YES;

                  // NSLog(@"Error = %@",error);
//                  [spinner stopAnimating];
//                  [hud hide:YES];
                  // [MBProgressHUD showErrorWithStatus:@"Something went wrong. Try again later"];

                  UIAlertView *alertview=[[UIAlertView alloc]
                                          initWithTitle:@""
                                          message:@"Something went wrong. Try again later"
                                          delegate:self
                                          cancelButtonTitle:@"OK"

                                          otherButtonTitles:nil];

                  [alertview show];
              }];



    }//end if

    else
    {
        UIAlertView *alertview=[[UIAlertView alloc]
                                initWithTitle:@""
                                message:@"No Network Available"
                                delegate:self
                                cancelButtonTitle:@"OK"

                                otherButtonTitles:nil];

        [alertview show];


    }//end else



}






#pragma mark - favoriteBtnAction()





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    
    
    
    SlectedViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"SlectedViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];

}

-(BOOL)prefersStatusBarHidden{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menubtn:(id)sender
{
    self.showmenuview.hidden = NO;
    self.hidemenuview.hidden = NO;
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = 0;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}

- (IBAction)menuhidebtn:(id)sender
{
    

    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = -265;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];

//    self.showmenuview.hidden = YES;
    self.hidemenuview.hidden = YES;


}
- (IBAction)setlocationbtn:(id)sender
{
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (IBAction)compass:(id)sender
{
    CompassViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"CompassViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
  
}

- (IBAction)weeklytable:(id)sender
{
    PrayertimeViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"PrayertimeViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    

}

- (IBAction)login:(id)sender
{
    UserNameViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"UserNameViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
}

- (IBAction)setting:(id)sender
{
    SettingViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
    
}
@end
