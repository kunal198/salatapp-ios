//
//  SlectedViewController.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlectedViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *PreviousPrayerLbl;
@property (strong, nonatomic) IBOutlet UIView *WrapperViewPreviousPrayer;
@property (strong, nonatomic) IBOutlet UIView *WrapperViewNextPrayer;
@property (strong, nonatomic) IBOutlet UILabel *NextPrayerTimeToStart;
- (IBAction)menubtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *menuhideoutlet;
@property (strong, nonatomic) IBOutlet UIView *showmenuview;
- (IBAction)setlocation:(id)sender;
- (IBAction)compassbtn:(id)sender;
- (IBAction)weeklytablebtn:(id)sender;
- (IBAction)loginbtn:(id)sender;
- (IBAction)settingbtn:(id)sender;




- (IBAction)nextbtn:(id)sender;
- (IBAction)menuhidebtn:(id)sender;
@end
