//
//  Friends&GurdainsViewController.m
//  SalatApp
//
//  Created by brst on 15/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "Friends&GurdainsViewController.h"
#import "ContactlistViewController.h"
@interface Friends_GurdainsViewController ()

@end

@implementation Friends_GurdainsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.friends.layer.cornerRadius = 25;
    self.friends.layer.masksToBounds = YES;
    self.friends.layer.borderWidth = 2;
    self.friends.layer.borderColor = [UIColor colorWithRed:109.0/255 green:47.0/255 blue:31.0/255 alpha:1.0].CGColor;
    
    
    self.gurdains.layer.cornerRadius = 25;
    self.gurdains.layer.masksToBounds = YES;
    self.gurdains.layer.borderWidth = 2;
    self.gurdains.layer.borderColor = [UIColor colorWithRed:109.0/255 green:47.0/255 blue:31.0/255 alpha:1.0].CGColor;

}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Gurdainbtn:(id)sender
{
    ContactlistViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactlistViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
  
}

- (IBAction)Friendbtn:(id)sender
{
    ContactlistViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactlistViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    

}
@end
