//
//  AllPrayersTableViewCell.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllPrayersTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *WrapperView;
@property (strong, nonatomic) IBOutlet UILabel *PrayerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *PrayerTimeLbl;
@property (strong, nonatomic) IBOutlet UIButton *WrapperBtn;

@property (strong, nonatomic) IBOutlet UIImageView *highlightring;
@property (strong, nonatomic) IBOutlet UIImageView *ring;
@property (strong, nonatomic) IBOutlet UILabel *dotlabel;

@end
