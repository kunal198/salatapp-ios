//
//  SettingViewController.m
//  SalatApp
//
//  Created by brst on 16/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.deleteaccoutlet.layer.cornerRadius = 20;
    self.deleteaccoutlet.layer.masksToBounds = YES;
    self.deleteaccoutlet.layer.borderWidth = 2;
    self.deleteaccoutlet.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    
    self.shareoutlet.layer.cornerRadius = 20;
    self.shareoutlet.layer.masksToBounds = YES;
    self.shareoutlet.layer.borderWidth = 2;
    self.shareoutlet.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    
    self.aboutoutlet.layer.cornerRadius = 20;
    self.aboutoutlet.layer.masksToBounds = YES;
    self.aboutoutlet.layer.borderWidth = 2;
    self.aboutoutlet.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;

}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)deleteaccountbtn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)sharefriendbtn:(id)sender {
}

- (IBAction)aboutbtn:(id)sender {
}
@end
