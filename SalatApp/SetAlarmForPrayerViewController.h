//
//  SetAlarmForPrayerViewController.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetAlarmForPrayerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *SetAlarmPrayerLbl;
@property (strong, nonatomic) IBOutlet UIButton *IprayBtn;
@property (strong, nonatomic) IBOutlet UIButton *SnoozeBtn;
- (IBAction)IprayBtn:(id)sender;
- (IBAction)SnoozeBtn:(id)sender;
- (IBAction)menubtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *showmenuview;

- (IBAction)hidemenubtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *menuhideoutlet;
- (IBAction)setlocationbtn:(id)sender;

- (IBAction)compassbtn:(id)sender;
- (IBAction)weeklytimetablebtn:(id)sender;
- (IBAction)loginbtn:(id)sender;
- (IBAction)settingbtn:(id)sender;


@end
