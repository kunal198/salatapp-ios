//
//  CompassViewController.m
//  SalatApp
//
//  Created by brst on 15/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "CompassViewController.h"

@interface CompassViewController ()

@end

@implementation CompassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.backoutlet.layer.cornerRadius = 30;
    self.backoutlet.layer.masksToBounds = YES;
    self.backoutlet.layer.borderWidth = 2;
    self.backoutlet.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backbtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}
@end
