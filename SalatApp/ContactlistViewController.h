//
//  ContactlistViewController.h
//  SalatApp
//
//  Created by brst on 14/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>

@interface ContactlistViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    bool isCheck;
    
}
@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UITableView *contactlisttable;

@property (strong, nonatomic) IBOutlet UITextField *searchtext;
- (IBAction)searchbtn:(id)sender;
@property (strong,nonatomic)NSMutableArray *contactlist,*dummyArraySelected;
@end
