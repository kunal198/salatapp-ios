//
//  UserNameViewController.m
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "UserNameViewController.h"
#import "GetUserIDViewController.h"

@interface UserNameViewController ()

@end

@implementation UserNameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = true;
    UINavigationBar *bar = [self.navigationController navigationBar];
    [bar setBarTintColor:[UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0]];
    [bar setTintColor:[UIColor whiteColor]];
    
    UIColor *color = [UIColor colorWithRed:181.0/255 green:148.0/255 blue:135.0/255 alpha:1.0];
    self.NameTextField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"Enter Username"
     attributes:@{NSForegroundColorAttributeName:color}];

    self.EnterBtn.layer.cornerRadius = 20;
    self.EnterBtn.layer.masksToBounds = YES;
    self.EnterBtn.layer.borderWidth = 2;
    self.EnterBtn.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    self.navigationItem.title = @"Prayer Snooze Social";
}

-(BOOL)prefersStatusBarHidden{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    return YES;
}


#pragma mark - textFieldShouldReturn()

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.NameTextField resignFirstResponder];
        return YES;
}



- (IBAction)EnterBtn:(id)sender
{
    GetUserIDViewController *userID = [self.storyboard instantiateViewControllerWithIdentifier:@"GetUserIDViewController"];
    [self.navigationController pushViewController:userID animated:NO];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
