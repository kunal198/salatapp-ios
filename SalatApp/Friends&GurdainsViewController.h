//
//  Friends&GurdainsViewController.h
//  SalatApp
//
//  Created by brst on 15/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Friends_GurdainsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *friends;
@property (strong, nonatomic) IBOutlet UIButton *gurdains;
- (IBAction)Gurdainbtn:(id)sender;

- (IBAction)Friendbtn:(id)sender;

@end
