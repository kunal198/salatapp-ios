//
//  UserNameViewController.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserNameViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *NameTextField;
@property (strong, nonatomic) IBOutlet UIView *UnderLineView;
@property (strong, nonatomic) IBOutlet UIButton *EnterBtn;
- (IBAction)EnterBtn:(id)sender;



@end
