//
//  SettingViewController.h
//  SalatApp
//
//  Created by brst on 16/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *deleteaccoutlet;
@property (strong, nonatomic) IBOutlet UIButton *shareoutlet;
@property (strong, nonatomic) IBOutlet UIButton *aboutoutlet;
- (IBAction)deleteaccountbtn:(id)sender;
- (IBAction)sharefriendbtn:(id)sender;
- (IBAction)aboutbtn:(id)sender;

@end
