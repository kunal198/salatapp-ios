//
//  AppDelegate.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
-(BOOL)internetActive;

@end

