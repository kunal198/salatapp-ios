//
//  PrayertimeViewController.m
//  SalatApp
//
//  Created by brst on 14/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "PrayertimeViewController.h"

@interface PrayertimeViewController ()

@end

@implementation PrayertimeViewController
@synthesize month;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = true;
    
    _daysscroll.scrollEnabled = true;
    
    //_daysscroll.contentSize = CGSizeMake(200, 191);
    
//    [_daysscroll setShowsVerticalScrollIndicator:YES];
//    [_daysscroll setContentSize:CGSizeMake(66, 191)];
       //_isPresented = NO;
    // Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews
{
    [_daysscroll setContentSize:CGSizeMake(0, month.frame.size.height * 11 )];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    return YES;
}


- (void)viewWillAppear:(BOOL)animated {
    
    CGAffineTransform rotateTransform;
    switch ([UIApplication sharedApplication].statusBarOrientation) {
        case UIDeviceOrientationLandscapeLeft:
            rotateTransform = CGAffineTransformMakeRotation(-90*M_PI/180.0);
            break;
        case UIDeviceOrientationLandscapeRight:
            rotateTransform = CGAffineTransformMakeRotation(90*M_PI/180.0);
            break;
            
        default:
            rotateTransform = CGAffineTransformMakeRotation(0);
            
            break;
    }
    
    rotateTransform = CGAffineTransformMakeRotation(-90*M_PI/180.0);
    
    [self.view setTransform:rotateTransform];
}


//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    CGAffineTransform t = self.view.transform;
//    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) && t.b && t.c) {
//        [self.view setTransform:CGAffineTransformMakeRotation(0)];
//    }
//}


- (IBAction)backbtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}
@end
