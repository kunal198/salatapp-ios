//
//  ViewController.m
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "ViewController.h"
#import "UserNameViewController.h"
#import "ContactlistViewController.h"
#import "Friends&GurdainsViewController.h"
#import <AddressBook/AddressBook.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = true;
    self.GoSocialBtn.layer.cornerRadius = 20;
    self.GoSocialBtn.layer.masksToBounds = YES;
    self.GoSocialBtn.layer.borderWidth = 2;
    self.GoSocialBtn.layer.borderColor = [UIColor colorWithRed:147.0/255 green:66.0/255 blue:36.0/255 alpha:1.0].CGColor;
}

-(BOOL)prefersStatusBarHidden{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    return YES;
}


- (IBAction)GoSocialBtn:(id)sender
{
    Friends_GurdainsViewController *username = [self.storyboard instantiateViewControllerWithIdentifier:@"Friends_GurdainsViewController"];
    
    [self.navigationController pushViewController:username animated:NO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
