//
//  ViewController.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIButton *GoSocialBtn;

- (IBAction)GoSocialBtn:(id)sender;


@end

