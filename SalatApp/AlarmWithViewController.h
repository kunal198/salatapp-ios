//
//  AlarmWithViewController.h
//  SalatApp
//
//  Created by brst on 4/14/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmWithViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *PrayerTitleLbl;

@property (strong, nonatomic) IBOutlet UIImageView *BackgroundImage;
@property (strong, nonatomic) IBOutlet UIButton *alone;
@property (strong, nonatomic) IBOutlet UIButton *jamaah;

@property (strong, nonatomic) IBOutlet UIButton *inmosque;
@property (strong, nonatomic) IBOutlet UIButton *witjfamily;

@property (strong, nonatomic) IBOutlet UIButton *backoutlet;
- (IBAction)backbtn:(id)sender;
- (IBAction)menubtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *showmenuview;
@property (strong, nonatomic) IBOutlet UIButton *menuhideoutlet;

- (IBAction)hidemenubtn:(id)sender;

- (IBAction)setlocationbtn:(id)sender;

- (IBAction)compassbtn:(id)sender;

- (IBAction)weeklytimetablebtn:(id)sender;
- (IBAction)loginbtn:(id)sender;
- (IBAction)settingbtn:(id)sender;

- (IBAction)alonebtn:(id)sender;

- (IBAction)jamaahbtn:(id)sender;

- (IBAction)inmosquebtn:(id)sender;

- (IBAction)withfamilybtn:(id)sender;
@end
