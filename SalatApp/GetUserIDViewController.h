//
//  GetUserIDViewController.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetUserIDViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *EnterBtn;
- (IBAction)EnterBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *UserIDLbl;
@property (strong, nonatomic) IBOutlet UILabel *UserIDTitle;

@end
