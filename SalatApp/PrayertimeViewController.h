//
//  PrayertimeViewController.h
//  SalatApp
//
//  Created by brst on 14/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrayertimeViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *daysscroll;
@property (nonatomic) BOOL isPresented;
- (IBAction)backbtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *subview;
@property (strong, nonatomic) IBOutlet UILabel *month;
@property (strong, nonatomic) IBOutlet UILabel *thrusday;
@property (strong, nonatomic) IBOutlet UILabel *friday;
@property (strong, nonatomic) IBOutlet UILabel *sunday;
@property (strong, nonatomic) IBOutlet UILabel *monday;
@property (strong, nonatomic) IBOutlet UILabel *tuesday;
@property (strong, nonatomic) IBOutlet UILabel *wednesday;

@end
