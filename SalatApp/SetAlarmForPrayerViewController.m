//
//  SetAlarmForPrayerViewController.m
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "SetAlarmForPrayerViewController.h"
#import "AlarmWithViewController.h"
#import "PrayertimeViewController.h"
#import "UserNameViewController.h"
#import "CompassViewController.h"
#import "SettingViewController.h"
@interface SetAlarmForPrayerViewController ()

@end

@implementation SetAlarmForPrayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationController.navigationBarHidden = true;
    self.SetAlarmPrayerLbl.layer.cornerRadius = 20;
    self.SetAlarmPrayerLbl.layer.masksToBounds = YES;
    self.SetAlarmPrayerLbl.layer.borderWidth = 2;
    self.SetAlarmPrayerLbl.layer.borderColor = [UIColor colorWithRed:242.0/255 green:229.0/255 blue:206.0/255 alpha:1.0].CGColor;
    
    self.IprayBtn.layer.cornerRadius = 20;
    self.IprayBtn.layer.masksToBounds = YES;
    self.IprayBtn.layer.borderWidth = 2;
    self.IprayBtn.layer.borderColor = [UIColor colorWithRed:109.0/255 green:47.0/255 blue:31.0/255 alpha:1.0].CGColor;
    
    
    self.SnoozeBtn.layer.cornerRadius = 20;
    self.SnoozeBtn.layer.masksToBounds = YES;
    self.SnoozeBtn.layer.borderWidth = 2;
    self.SnoozeBtn.layer.borderColor = [UIColor colorWithRed:109.0/255 green:47.0/255 blue:31.0/255 alpha:1.0].CGColor;
    self.menuhideoutlet.hidden = YES;
    self.navigationItem.title = @"Prayer Snooze Social";
    self.navigationItem.backBarButtonItem.title=@"Back";
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.showmenuview.hidden = YES;
    self.menuhideoutlet.hidden = YES;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)IprayBtn:(id)sender
{
    AlarmWithViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"AlarmWithViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    

}

- (IBAction)SnoozeBtn:(id)sender
{
      
}

- (IBAction)menubtn:(id)sender
{
    self.showmenuview.hidden = NO;
    self.menuhideoutlet.hidden = NO;
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = 0;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];

}

- (IBAction)hidemenubtn:(id)sender
{
    
   
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = -265;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
     self.menuhideoutlet.hidden = YES;

}
- (IBAction)setlocationbtn:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (IBAction)compassbtn:(id)sender
{
    CompassViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"CompassViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
}

- (IBAction)weeklytimetablebtn:(id)sender
{
    PrayertimeViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"PrayertimeViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    

}

- (IBAction)loginbtn:(id)sender
{
    UserNameViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"UserNameViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    

}

- (IBAction)settingbtn:(id)sender
{
    SettingViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
  
    
}
@end
