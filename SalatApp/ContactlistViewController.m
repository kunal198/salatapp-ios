//
//  ContactlistViewController.m
//  SalatApp
//
//  Created by brst on 14/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "ContactlistViewController.h"
#import "ContactlistTableViewCell.h"
#import "AllPrayersViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>



@interface ContactlistViewController ()

@end

@implementation ContactlistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   self.navigationController.navigationBarHidden = true;
    isCheck = false;
    self.contactlist = [[NSMutableArray alloc]init];
    
    self.contactlisttable.delegate = self;
    
   // self.contactlist = [NSArray arrayWithObjects:@"Mark k",@"Rock j",@"Harry l",@"Killy g",@"Mark k",@"Rock j",@"Harry l", nil];
    
    
    self.navigationController.navigationBarHidden = true;
//    self.searchtext.layer.cornerRadius = 20;
//    self.searchtext.layer.masksToBounds = YES;
//    self.searchtext.layer.borderWidth = 1;
    self.searchtext.backgroundColor = [UIColor whiteColor];
//    self.searchtext.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    

    self.searchView.layer.cornerRadius = 20;
    self.searchView.layer.masksToBounds = YES;
    self.searchView.layer.borderWidth = 1;
    self.searchView.backgroundColor = [UIColor whiteColor];
    self.searchView.layer.borderColor = [UIColor lightGrayColor].CGColor;

    ABAddressBookRef addressBook = ABAddressBookCreate();
    
    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL)
    {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        //dispatch_release(semaphore);
    }
    
    else { // We are on iOS 5 or Older
        accessGranted = YES;
        [self getContactsWithAddressBook:addressBook];
    }
    
    if (accessGranted) {
        [self getContactsWithAddressBook:addressBook];
    }
    

    
    
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)getContactsWithAddressBook:(ABAddressBookRef )addressBook {
    
    _contactlist = [[NSMutableArray alloc] init];
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i=0;i < nPeople;i++) {
        NSMutableDictionary *dOfPerson=[NSMutableDictionary dictionary];
        
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
        
        //For username and surname
        ABMultiValueRef phones =(__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonPhoneProperty));
        
        CFStringRef firstName;
        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        //lastName  = ABRecordCopyValue(ref, kABPersonLastNameProperty);
        [dOfPerson setObject:[NSString stringWithFormat:@"%@ ", firstName] forKey:@"name"];
        
        //For Email ids
        ABMutableMultiValueRef eMail  = ABRecordCopyValue(ref, kABPersonEmailProperty);
        if(ABMultiValueGetCount(eMail) > 0) {
            //[dOfPerson setObject:(__bridge NSString *)ABMultiValueCopyValueAtIndex(eMail, 0) forKey:@"email"];
            
        }
        
        //For Phone number
        NSString* mobileLabel;
        
        for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++) {
            mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, j);
            if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, j) forKey:@"Phone"];
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, j) forKey:@"Phone"];
                break ;
            }
            
        }
        [_contactlist addObject:dOfPerson];
        
    }
    NSLog(@"Contacts = %@",_contactlist);
}




-(BOOL)prefersStatusBarHidden
    {
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [self.contactlist count];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactlistTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    
    cell.name.text = [_contactlist[indexPath.row]valueForKey:@"name"];
    cell.mobilenumber.text = [_contactlist[indexPath.row]valueForKey:@"Phone"];
    cell.unmarkimage.image = [UIImage imageNamed:@"unmark.png"];
    
//    if (isCheck == false)
//    {
//        NSLog(@"unchecked selected");
//        cell.unmarkimage.image = [UIImage imageNamed:@"mark.png"];
//        isCheck = true;
//    }
//    else
//    {
//        NSLog(@"checked selected");
//        cell.unmarkimage.image = [UIImage imageNamed:@"unmark.png"];
//        isCheck = false;
//    }
    
    
    
////        if ([_dummyArraySelected containsObject:[_contactlist objectAtIndex:indexPath.row]])
////        {
//            cell.markimage.image = [UIImage imageNamed:@"mark.png"];
////        }
//        else
//        {
//            cell.unmarkimage.image = [UIImage imageNamed:@"unmark.png"];
//        }
//        
//       
//        
//        
//        
//        tableView.separatorInset = UIEdgeInsetsZero;
        
        // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    
    
    
       return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactlistTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//    if ([_dummyArraySelected containsObject:[_contactlist objectAtIndex:indexPath.row]])
//    {
//        NSLog(@"%ld",(long)indexPath.row);
//        NSLog(@"dummyArraySelected %@",_dummyArraySelected);
//        
//        [_dummyArraySelected removeObject:[_contactlist objectAtIndex:indexPath.row]];
//       cell.markimage.image = [UIImage imageNamed:@"unmark.png"];
//        
//    }
//    else
//    {
//        [_dummyArraySelected addObject:[_contactlist objectAtIndex:indexPath.row]];
//        cell.unmarkimage.image = [UIImage imageNamed:@"mark.png"];
//        NSLog(@"%ld",(long)indexPath.row);
//        NSLog(@"dummyArraySelected %@",_dummyArraySelected);
//        
//    }
    
//    selectedIndustry = [_contactlist objectAtIndex:indexPath.row];
//    
//    arr = [NSMutableArray arrayWithArray:_dummyArraySelected];
//    joinedString = [arr componentsJoinedByString:@"~"];
//    
//    NSLog(@"joinedString is %@ arr is %lu",joinedString,(unsigned long)arr.count);
//    
    if (isCheck == false)
    {
        cell.markimage.hidden = true;
        cell.unmarkimage.hidden = false;
        isCheck = true;

//    [_contactlisttable reloadData];

    }
    else{
        cell.markimage.hidden = false;
        cell.unmarkimage.hidden = true;
        isCheck = false;
    }

}



- (IBAction)DoneBtn:(id)sender
{
    
    AllPrayersViewController *username = [self.storyboard instantiateViewControllerWithIdentifier:@"AllPrayersViewController"];
    [self.navigationController pushViewController:username animated:NO];
    
}











- (IBAction)searchbtn:(id)sender
{
    [sender resignFirstResponder];
}
@end
