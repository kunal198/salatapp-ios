//
//  AlarmWithViewController.m
//  SalatApp
//
//  Created by brst on 4/14/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "AlarmWithViewController.h"
#import "CompassViewController.h"
#import "PrayertimeViewController.h"
#import "UserNameViewController.h"
#import "SettingViewController.h"
@interface AlarmWithViewController ()

@end

@implementation AlarmWithViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = true;
    
    self.PrayerTitleLbl.layer.cornerRadius = 25;
    self.PrayerTitleLbl.layer.masksToBounds = YES;
    self.PrayerTitleLbl.layer.borderWidth = 2;
    self.PrayerTitleLbl.layer.borderColor = [UIColor colorWithRed:146.0/255 green:63.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    self.alone.layer.cornerRadius = 15;
    self.alone.layer.masksToBounds = YES;
    self.alone.layer.borderWidth = 2;
    self.alone.layer.borderColor = [UIColor colorWithRed:146.0/255 green:63.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    self.jamaah.layer.cornerRadius = 15;
    self.jamaah.layer.masksToBounds = YES;
    self.jamaah.layer.borderWidth = 2;
    self.jamaah.layer.borderColor = [UIColor colorWithRed:146.0/255 green:63.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    
    self.inmosque.layer.cornerRadius = 15;
    self.inmosque.layer.masksToBounds = YES;
    self.inmosque.layer.borderWidth = 2;
    self.inmosque.layer.borderColor = [UIColor colorWithRed:146.0/255 green:63.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    self.witjfamily.layer.cornerRadius = 15;
    self.witjfamily.layer.masksToBounds = YES;
    self.witjfamily.layer.borderWidth = 2;
    self.witjfamily.layer.borderColor = [UIColor colorWithRed:146.0/255 green:63.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    
    self.backoutlet.layer.cornerRadius = 25;
    self.backoutlet.layer.masksToBounds = YES;
    self.backoutlet.layer.borderWidth = 2;
    self.backoutlet.layer.borderColor = [UIColor colorWithRed:146.0/255 green:63.0/255 blue:36.0/255 alpha:1.0].CGColor;
    
    self.navigationItem.title = @"Prayer Snooze Social";
    self.navigationItem.backBarButtonItem.title=@"Back";
   self.menuhideoutlet.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.showmenuview.hidden = YES;
    self.menuhideoutlet.hidden = YES;
    
    
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)backbtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)menubtn:(id)sender
{
    self.showmenuview.hidden = NO;
    self.menuhideoutlet.hidden = NO;
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = 0;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];

}
- (IBAction)hidemenubtn:(id)sender
{
    
    
    [UIView animateWithDuration:1
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         CGRect frame = self.showmenuview.frame;
         frame.origin.y = 0;
         frame.origin.x = -265;
         _showmenuview.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];

    
       self.menuhideoutlet.hidden = YES;
}

- (IBAction)setlocationbtn:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (IBAction)compassbtn:(id)sender
{
    
    CompassViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"CompassViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
}

- (IBAction)weeklytimetablebtn:(id)sender
{
    PrayertimeViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"PrayertimeViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];

}

- (IBAction)loginbtn:(id)sender
{
    UserNameViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"UserNameViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    
}

- (IBAction)settingbtn:(id)sender
{
    SettingViewController *selectedPrayer = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [self.navigationController pushViewController:selectedPrayer animated:NO];
    

}

- (IBAction)alonebtn:(id)sender {
}

- (IBAction)jamaahbtn:(id)sender {
}

- (IBAction)inmosquebtn:(id)sender {
}

- (IBAction)withfamilybtn:(id)sender {
}



@end
