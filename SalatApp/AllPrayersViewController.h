//
//  AllPrayersViewController.h
//  SalatApp
//
//  Created by brst on 4/13/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface AllPrayersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>
{
    NSMutableArray *AllPrayersName,*AllPrayerTime;
    NSString *strUrlLink;
    BOOL isSelectBtn;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    int locationFetchCounter;
    NSString *city;
    
}

@property (strong, nonatomic) IBOutlet UITableView *AllPrayersTableView;
- (IBAction)menubtn:(id)sender;
- (IBAction)menuhidebtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *showmenuview;
@property (strong, nonatomic) IBOutlet UIButton *hidemenuview;
- (IBAction)setlocationbtn:(id)sender;
- (IBAction)compass:(id)sender;
- (IBAction)weeklytable:(id)sender;
- (IBAction)login:(id)sender;
- (IBAction)setting:(id)sender;

@end
